# language: es

Característica: Realizar la reserva de viaje en tren

  Escenario: Acceder a la pagina de prurail para la reserva de viajes en tren
    Dado que un usuario accede a la pagina
    Cuando el usuario llena los datos necesarios
    Entonces seguira el paso dos
    Dado la cantidad de cabinas
    Cuando viajen las personas
    Entonces seguira el paso tres
    Dado que en la cabina uno van dos adultos
    Cuando llenamos los datos del pasajero uno (Sebastian, Torres, fecha de nacimiento, nacionalidad, tipo de id, 1022435627, genero, pais del telefono, 3203096741, sebastian15320@hotmail.com, sebastian15320@hotmail.com, aceptar ofertas)
    Cuando llenamos los datos del pasajero dos (Juan, Rincón, fecha de nacimiento, nacionalidad, tipo de id, 1025964785, genero)
    Dado que en la cabina van dos niños
    Cuando llenamos los datos del pasajero tres (Yuly, Torres, fecha de nacimiento, nacionalidad, tipo de id, 101056874, genero)
    Cuando llenamos los datos del pasajero cuatro (Luciana, Novoa, fecha de nacimiento, nacionalidad, tipo de id, 101056875, genero)
    Entonces seguira el paso cuatro
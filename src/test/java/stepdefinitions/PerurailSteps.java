package stepdefinitions;

import com.ibm.icu.impl.number.formatters.StrongAffixFormat;
import cucumber.api.java.es.*;
import jnr.x86asm.REG;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Open;
import net.thucydides.core.annotations.Managed;
import org.openqa.selenium.WebDriver;
import tasks.*;
import userinterface.HomePage;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isPresent;
import static net.serenitybdd.screenplay.questions.WebElementQuestion.the;
import static userinterface.SolicitudViaje.*;

public class PerurailSteps {
    @Managed(driver = "chrome")
    public WebDriver webDriver;
    public Actor actor = Actor.named("Juan");
    public HomePage homePage = new HomePage();
    @Dado("^que un usuario accede a la pagina$")
    public void queUnUsuarioAccedeALaPagina() {
        actor.can(BrowseTheWeb.with(webDriver));
        actor.wasAbleTo(Open.browserOn(homePage));
    }
    @Cuando("^el usuario llena los (.*) necesarios$")
    public void elUsuarioLlenaLosDatosNecesarios(String viaje) {actor.wasAbleTo(BuscarViaje.elViaje(viaje));}
    @Entonces("^seguira el paso dos$")
    public void seguiraElPasoDos() {
        actor.should(seeThat(the(SOLICITUD_VIAJE), isPresent()));
    }
    @Dado("^la cantidad de (.*)$")
    public void laCantidadDePersonas(String cabina) {
        actor.wasAbleTo(Cabinas.lasCabinas(cabina));
    }
    @Cuando("^viajen las (.*)$")
    public void seComprenLosBoletos(String persona) {
        actor.wasAbleTo(Personas.laspersonas(persona));
    }
    @Entonces("^seguira el paso tres$")
    public void seguiraElPasoTres() {
        actor.should(seeThat(the(SOLICITUD_CABINAS_PERSONAS), isPresent()));
    }
    @Dado("^que en la cabina uno van dos adultos$")
    public void queEnLaCabinaUnoVanDosAdultos() {
        actor.should(seeThat(the(REGISTRO_PASAJEROS), isPresent()));
    }
    @Cuando("^llenamos los datos del pasajero uno \\((.*), (.*), fecha de nacimiento, nacionalidad, tipo de id, (.*), genero, pais del telefono, (.*), (.*), (.*), aceptar ofertas\\)$")
    public void llenamosLosDatosDelPasajeroUnoNompreApellidoFechaDeNacimientoNacionalidadTipoDeIdIdGeneroPaisDelTelefonoTelefonoEMailConfirmarEMailAceptarOfertas(String nombrePersonaUno, String apellidoPersonaUno, String documentoPersonaUno, String numeroTelefonoUno, String emailUno, String confirmarEmailUno) {
        actor.wasAbleTo(DatosUno.datosPersonaUno(nombrePersonaUno, apellidoPersonaUno, documentoPersonaUno, numeroTelefonoUno, emailUno, confirmarEmailUno));
    }
    @Cuando("^llenamos los datos del pasajero dos \\((.*), (.*), fecha de nacimiento, nacionalidad, tipo de id, (.*), genero\\)$")
    public void llenamosLosDatosDelPasajeroDosNompreApellidoFechaDeNacimientoNacionalidadTipoDeIdIdGenero(String nombrePersonaDos, String apellidoPersonaDos, String documentoPersonaDos) {
        actor.wasAbleTo((DatosDos.datosPersonaDos(nombrePersonaDos, apellidoPersonaDos, documentoPersonaDos)));
    }
    @Dado("^que en la cabina van dos niños$")
    public void queEnLaCabinaVanDosNiños() {
        actor.should(seeThat(the(REGISTRO_PASAJEROS), isPresent()));
    }
    @Cuando("^llenamos los datos del pasajero tres \\((.*), (.*), fecha de nacimiento, nacionalidad, tipo de id, (.*), genero\\)$")
    public void llenamosLosDatosDelPasajeroTresNompreApellidoFechaDeNacimientoNacionalidadTipoDeIdIdGenero(String nombrePersonaTres, String apellidoPersonaTres, String documentoPersonaTres) {
        actor.wasAbleTo(DatosTres.datosPersonaTres(nombrePersonaTres, apellidoPersonaTres, documentoPersonaTres));
    }
    @Cuando("^llenamos los datos del pasajero cuatro \\((.*), (.*), fecha de nacimiento, nacionalidad, tipo de id, (.*), genero\\)$")
    public void llenamosLosDatosDelPasajeroCuatroNompreApellidoFechaDeNacimientoNacionalidadTipoDeIdIdGenero(String nombrePersonaCuatro, String apellidoPersonaCuatro, String documentoPersonaCuatro) {
        actor.wasAbleTo(DatosCuatro.datosPersonaCuatro(nombrePersonaCuatro, apellidoPersonaCuatro, documentoPersonaCuatro));
    }
    @Entonces("^seguira el paso cuatro$")
    public void seguiraElPasoCuatro() {
        //actor.should(seeThat(the(PAGO_VIAJE), isPresent()));
    }
}
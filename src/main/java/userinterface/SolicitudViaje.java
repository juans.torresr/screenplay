package userinterface;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class SolicitudViaje {
    public static final Target SOLICITUD_VIAJE = Target.the("Solicitud del viaje").located(By.className("content-ticket"));
    public static final Target SOLICITUD_CABINAS_PERSONAS = Target.the("Solicitud de las cabinas y las personas").located(By.id("pasajeros"));
    public static final Target REGISTRO_PASAJEROS = Target.the("Registro de todas las personas").located(By.className("datos-pasajero"));
    public static final Target PAGO_VIAJE = Target.the("Pago del viaje total").located(By.className("lista-medio-pago"));
}

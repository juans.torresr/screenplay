package userinterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class DatosPersonaUno extends PageObject {
    public static final Target NOMBRE_PERSONA_UNO = Target.the("Nombre de la persona numero uno").located(By.id("txt_nombre[twin][cab1][1]"));
    public static final Target APELLIDO_PERSONA_UNO = Target.the("Apellido de la persona numero uno").located(By.id("txt_apellido[twin][cab1][1]"));
    public static final Target FECHA_NACIMIENTO = Target.the("Fecha de nacimiento de la persona").located(By.id("txt_fecha_nacimiento[twin][cab1][1]"));
    public static final Target AÑO_NACIMIENTO = Target.the("Año de nacimiento de la persona").locatedBy("//*[@id=\"ui-datepicker-div\"]/div/div/select[2]");
    public static final Target SELECCION_AÑO_NACIMIENTO = Target.the("Selección del año de nacimiento de la persona").locatedBy("//*[@id=\"ui-datepicker-div\"]/div/div/select[2]/option[77]");
    public static final Target MES_NACIMIENTO = Target.the("Mes de nacimiento de la persona").locatedBy("//*[@id=\"ui-datepicker-div\"]/div/div/select[1]");
    public static final Target SELECCION_MES_NACIMIENTO = Target.the("Seleccion del mes de nacimiento").locatedBy("//*[@id=\"ui-datepicker-div\"]/div/div/select[1]/option[5]");
    public static final Target SELECCION_DIA_NACIMIENTO = Target.the("Selección del día de nacimiento").locatedBy("//*[@id=\"ui-datepicker-div\"]/table/tbody/tr[5]/td[3]/a");
    public static final Target NACIONALIDAD = Target.the("Nacionalidad de la persona").located(By.id("sel_nacion[twin][cab1][1]"));
    public static final Target CIUDAD_NACIONALIDDAD = Target.the("Ciudad de la nacionalidad de la persona").locatedBy("//*[@id=\"sel_nacion[twin][cab1][1]\"]/option[49]");
    public static final Target TIPO_DOCUMENTO = Target.the("Tipo de documento de la persona").located(By.id("sel_tpdoc[twin][cab1][1]"));
    public static final Target SELECCION_TIPO_DOCUMENTO = Target.the("Selección del tipo de documento de la persona").locatedBy("//*[@id=\"sel_tpdoc[twin][cab1][1]\"]/option[3]");
    public static final Target NUMERO_DOCUMENTO = Target.the("Número de documento de la persona").located(By.name("txt_nroid[twin][cab1][1]"));
    public static final Target GENERO = Target.the("Genero de la persona").located(By.id("sel_sexo[twin][cab1][1]"));
    public static final Target TIPO_GENERO = Target.the("Tipo de genero de la persona").locatedBy("//*[@id=\"sel_sexo[twin][cab1][1]\"]/option[2]");
    public static final Target PAIS_TELEFONO = Target.the("País del número de teléfono").locatedBy("//*[@id=\"divcab-1\"]/div[2]/div[1]/div[2]/div/div/div");
    public static final Target SELECCION_PAIS_TELEFONO = Target.the("Selección del país del número de telefono").locatedBy("//*[@id=\"divcab-1\"]/div[2]/div[1]/div[2]/div/div/ul/li[50]/span[1]");
    public static final Target NUMERO_TELEFONO = Target.the("Número de teléfono de la persona").located(By.name("txt_telefono[twin][cab1][1]"));
    public static final Target EMAIL = Target.the("Email de la persona").located(By.name("txt_mail[twin][cab1][1]"));
    public static final Target CONFIRMAR_EMAIL = Target.the("Confirmar el email de la persona").located(By.name("txt_mail_conf[twin][cab1][1]"));
    public static final Target OFERTAS_EMAIL = Target.the("Ofertas que llegan al email").located(By.id("chk_ofertas[twin][cab1][1]"));
    public static final Target SIGUIENTE_PERSONA = Target.the("Siguiente persona para llenar datos").located(By.id("itm1-2"));
}

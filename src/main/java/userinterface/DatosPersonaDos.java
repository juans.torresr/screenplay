package userinterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class DatosPersonaDos extends PageObject {
    public static final Target NOMBRE_PERSONA_DOS = Target.the("Nombre de la persona número dos").located(By.name("txt_nombre[twin][cab1][2]"));
    public static final Target APELLIDO_PERSONA_DOS =  Target.the("Apellido de la persona número dos").located(By.name("txt_apellido[twin][cab1][2]"));
    public static final Target FECHA_NACIMIENTO = Target.the("Fecha de nacimiento de la persona").located(By.name("txt_fecha_nacimiento[twin][cab1][2]"));
    public static final Target AÑO_NACIMIENTO = Target.the("Año de nacimiento de la persona").locatedBy("//*[@id=\"ui-datepicker-div\"]/div/div/select[2]");
    public static final Target SELECCION_AÑO_NACIMIENTO = Target.the("Selección del año de nacimiento de la persona").locatedBy("//*[@id=\"ui-datepicker-div\"]/div/div/select[2]/option[3]");
    public static final Target MES_NACIMIENTO = Target.the("Mes de nacimiento de la persona").locatedBy("//*[@id=\"ui-datepicker-div\"]/div/div/select[1]");
    public static final Target SELECCION_MES_NACIMIENTO = Target.the("Seleccion del mes de nacimiento").locatedBy("//*[@id=\"ui-datepicker-div\"]/div/div/select[1]/option[8]");
    public static final Target SELECCION_DIA_NACIMIENTO = Target.the("Selección del día de nacimiento").locatedBy("//*[@id=\"ui-datepicker-div\"]/table/tbody/tr[4]/td[6]/a");
    public static final Target NACIONALIDAD = Target.the("Nacionalidad de la persona").located(By.id("sel_nacion[twin][cab1][2]"));
    public static final Target CIUDAD_NACIONALIDDAD = Target.the("Ciudad de la nacionalidad de la persona").locatedBy("//*[@id=\"sel_nacion[twin][cab1][2]\"]/option[49]");
    public static final Target TIPO_DOCUMENTO = Target.the("Tipo de documento de la persona").located(By.id("sel_tpdoc[twin][cab1][2]"));
    public static final Target SELECCION_TIPO_DOCUMENTO = Target.the("Selección del tipo de documento de la persona").locatedBy("//*[@id=\"sel_tpdoc[twin][cab1][2]\"]/option[3]");
    public static final Target NUMERO_DOCUMENTO = Target.the("Número de documento de la persona").located(By.name("txt_nroid[twin][cab1][2]"));
    public static final Target GENERO = Target.the("Genero de la persona").located(By.id("sel_sexo[twin][cab1][2]"));
    public static final Target TIPO_GENERO = Target.the("Tipo de genero de la persona").locatedBy("//*[@id=\"sel_sexo[twin][cab1][2]\"]/option[2]");
    public static final Target SIGUIENTE_CABINA = Target.the("Siguiete cabina").located(By.id("cab-2"));
    public static final Target SIGUIENTE_PERSONA = Target.the("Siguiete persona").located(By.id("itm2-1"));

}

package userinterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;

@DefaultUrl("https://www.perurail.com/")
public class HomePage extends PageObject {

    public static final Target IMAGEN_PAGINA = Target.the("Imagen de la pagina").located(By.className("logo"));
    public static final Target SELECCIONAR_TIPO_VIAJE = Target.the("Seleccionar el tipo de viaje").locatedBy("/html/body/div[1]/div[3]/div[2]/form/div/div[1]/div[2]/label/span");
    public static final Target VIAJE_IDA = Target.the("Viaje de ida").located(By.id("Filtros_Ida_Origen"));
    public static final Target DESTINO_IDA = Target.the("Destino de ida").locatedBy("//*[@id=\"Filtros_Ida_Origen\"]/optgroup[1]/option[1]");
    public static final Target SELECCIONAR_RUTA_VIAJE = Target.the("Sleccionar la ruta del viaje").located(By.id("Filtros_Ida_Destino"));
    public static final Target RUTA_VIAJE = Target.the("Ruta de viaje").locatedBy("//*[@id=\"Filtros_Ida_Destino\"]/optgroup[1]/option");
    public static final Target SERVICIO_TERN = Target.the("Servidio de tren").located(By.className("main-mensaje-inner"));
    public static final Target BUSCAR_VIAJE = Target.the("Buscar viaje").located(By.id("btn_search"));

}

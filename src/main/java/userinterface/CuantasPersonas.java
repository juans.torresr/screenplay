package userinterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class CuantasPersonas extends PageObject {
    public static final Target CABINA1_NIÑOS =  Target.the("Cabina uno para niños").locatedBy("//select[@class=\"baeRoomChild1\"]");
    public static final Target CANTIDAD_NIÑOS = Target.the("Cantidad de niños en la cabina").locatedBy("//*[@id=\"twin\"]/div[1]/div[2]/div[2]/select/option[2]");
    public static final Target CABINA2_ADULTOS = Target.the("Cabina dos para adultos").locatedBy("//select[@class=\"baeRoomChild1\"]");
    public static final Target CANTIDAD_ADULTOS = Target.the("Cantidad de adultos en la cabina").locatedBy("//*[@id=\"twin\"]/div[2]/div[2]/div[2]/select/option[3]");
    public static final Target SUBTOTAL = Target.the("Subtotal del valor").located(By.id("tarifaSUBTOTAL"));
    public static final Target SELECCIONAR_BOTON = Target.the("Seleccionar botón continuar").located(By.id("continuar_bae"));
}

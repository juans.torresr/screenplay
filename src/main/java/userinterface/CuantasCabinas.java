package userinterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class CuantasCabinas extends PageObject {
    public static final Target SELECCION_CABINA = Target.the("Seleccionando la cabina").located(By.name("selectRooms[twin]"));
    public static final Target CANTIDAD_CABINAS = Target.the("Cantidad de cabinas").locatedBy("//*[@id=\"frm_viajes_bae\"]/div[3]/div[2]/div[3]/div[3]/div/div[1]/select/option[3]");
}

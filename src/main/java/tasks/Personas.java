package tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.questions.Visibility;
import static net.serenitybdd.screenplay.Tasks.instrumented;
import static userinterface.CuantasPersonas.*;

public class Personas implements Task {
    String persona;
    public Personas(String persona){
        this.persona = persona;
    }
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(CABINA1_NIÑOS), Click.on(CANTIDAD_NIÑOS), Click.on(CABINA2_ADULTOS), Click.on(CANTIDAD_ADULTOS));
        if (Visibility.of(SUBTOTAL).viewedBy(actor).asBoolean()){
            actor.attemptsTo(Click.on(SELECCIONAR_BOTON));
        }

    }
    public static Personas laspersonas(String persona){
        return instrumented(Personas.class, persona);
    }
}

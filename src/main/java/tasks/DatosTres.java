package tasks;

import com.ibm.icu.impl.number.formatters.StrongAffixFormat;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.questions.Visibility;
import static net.serenitybdd.screenplay.Tasks.instrumented;
import static userinterface.DatosPersonaTres.*;

public class DatosTres implements Task {
    String nombrePersonaTres;
    String apellidoPersonaTres;
    String documentoPersonaTres;
    public DatosTres(String nombrePersonaTres, String apellidoPersonaTres, String documentoPersonaTres){
        this.nombrePersonaTres = nombrePersonaTres;
        this.apellidoPersonaTres = apellidoPersonaTres;
        this.documentoPersonaTres = documentoPersonaTres;
    }
    @Override
    public <T extends Actor> void performAs(T actor) {
        if (Visibility.of(NOMBRE_PERSONA_TRES).viewedBy(actor).asBoolean()){
            actor.attemptsTo(Enter.theValue(nombrePersonaTres).into(NOMBRE_PERSONA_TRES), Enter.theValue(apellidoPersonaTres).into(APELLIDO_PERSONA_TRES),
                    Click.on(FECHA_NACIMIENTO), Click.on(AÑO_NACIMIENTO), Click.on(SELECCION_AÑO_NACIMIENTO), Click.on(MES_NACIMIENTO),
                    Click.on(SELECCION_MES_NACIMIENTO), Click.on(SELECCION_DIA_NACIMIENTO), Click.on(NACIONALIDAD), Click.on(CIUDAD_NACIONALIDDAD),
                    Click.on(TIPO_DOCUMENTO), Click.on(SELECCION_TIPO_DOCUMENTO), Enter.theValue(documentoPersonaTres).into(NUMERO_DOCUMENTO),
                    Click.on(GENERO), Click.on(TIPO_GENERO), Click.on(SIGUIENTE_PERSONA));
        }
    }
    public static DatosTres datosPersonaTres(String nombrePersonaTres, String apellidoPersonaTres, String documentoPersonaTres){
        return instrumented(DatosTres.class, nombrePersonaTres, apellidoPersonaTres, documentoPersonaTres);
    }
}

package tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.questions.Visibility;
import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.thucydides.core.webdriver.ThucydidesWebDriverSupport.getDriver;
import static userinterface.HomePage.*;

public class BuscarViaje implements Task {
    private String viaje;
    public BuscarViaje(String viaje){this.viaje = viaje;}
    @Override
    public <T extends Actor> void performAs(T actor) {
        if (Visibility.of(IMAGEN_PAGINA).viewedBy(actor).asBoolean()){
            actor.attemptsTo(Click.on(SELECCIONAR_TIPO_VIAJE), Click.on(VIAJE_IDA), Click.on(DESTINO_IDA), Click.on(SELECCIONAR_RUTA_VIAJE), Click.on(RUTA_VIAJE));
            if (Visibility.of(SERVICIO_TERN).viewedBy(actor).asBoolean()){
                actor.attemptsTo((Click.on(BUSCAR_VIAJE)));
            }
            for (String winHandle : getDriver().getWindowHandles()){
                getDriver().switchTo().window(winHandle);
            }
        }
    }
    public static BuscarViaje elViaje(String viaje){return instrumented(BuscarViaje.class, viaje);}
}

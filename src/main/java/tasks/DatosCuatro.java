package tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.questions.Visibility;
import static java.lang.Thread.sleep;
import static net.serenitybdd.screenplay.Tasks.instrumented;
import static userinterface.DatosPersonaCuatro.*;
public class DatosCuatro implements Task {
    String nombrePersonaCuatro;
    String apellidoPersonaCuatro;
    String documentoPersonaCuatro;
    public DatosCuatro(String nombrePersonaCuatro, String apellidoPersonaCuatro, String documentoPersonaCuatro){
        this.nombrePersonaCuatro = nombrePersonaCuatro;
        this.apellidoPersonaCuatro = apellidoPersonaCuatro;
        this.documentoPersonaCuatro = documentoPersonaCuatro;
    }
    @Override
    public <T extends Actor> void performAs(T actor) {
        if (Visibility.of(NOMBRE_PERSONA_CUATRO).viewedBy(actor).asBoolean()){
            actor.attemptsTo(Enter.theValue(nombrePersonaCuatro).into(NOMBRE_PERSONA_CUATRO), Enter.theValue(apellidoPersonaCuatro).into(APELLIDO_PERSONA_CUATRO),
                    Click.on(FECHA_NACIMIENTO), Click.on(AÑO_NACIMIENTO), Click.on(SELECCION_AÑO_NACIMIENTO), Click.on(MES_NACIMIENTO),
                    Click.on(SELECCION_MES_NACIMIENTO), Click.on(SELECCION_DIA_NACIMIENTO), Click.on(NACIONALIDAD), Click.on(CIUDAD_NACIONALIDDAD),
                    Click.on(TIPO_DOCUMENTO), Click.on(TIPO_DOCUMENTO), Enter.theValue(documentoPersonaCuatro).into(NUMERO_DOCUMENTO),
                    Click.on(GENERO), Click.on(TIPO_GENERO), Click.on(SIGUIENTE_PASO));
        }
        try {
            sleep(5000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
    public static DatosCuatro datosPersonaCuatro(String nombrePersonaCuatro, String apellidoPersonaCuatro, String documentoPersonaCuatro){
        return instrumented(DatosCuatro.class, nombrePersonaCuatro, apellidoPersonaCuatro, documentoPersonaCuatro);
    }
}

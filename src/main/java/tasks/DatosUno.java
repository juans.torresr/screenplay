package tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.questions.Visibility;
import static net.serenitybdd.screenplay.Tasks.instrumented;
import static userinterface.DatosPersonaUno.*;
public class DatosUno implements Task {
    String nombrePersonaUno;
    String apellidoPersonaUno;
    String documentoPersonaUno;
    String numeroTelefonoUno;
    String emailUno;
    String confirmarEmailUno;
    public DatosUno(String nombrePersonaUno, String apellidoPersonaUno, String documentoPersonaUno, String numeroTelefonoUno, String emailUno, String confirmarEmailUno){
        this.nombrePersonaUno = nombrePersonaUno;
        this.apellidoPersonaUno = apellidoPersonaUno;
        this.documentoPersonaUno = documentoPersonaUno;
        this.numeroTelefonoUno = numeroTelefonoUno;
        this.emailUno = emailUno;
        this.confirmarEmailUno = confirmarEmailUno;
    }
    @Override
    public <T extends Actor> void performAs(T actor) {
        if (Visibility.of(NOMBRE_PERSONA_UNO).viewedBy(actor).asBoolean()){
            actor.attemptsTo(Enter.theValue(nombrePersonaUno).into(NOMBRE_PERSONA_UNO), Enter.theValue(apellidoPersonaUno).into(APELLIDO_PERSONA_UNO),
                    Click.on(FECHA_NACIMIENTO), Click.on(AÑO_NACIMIENTO), Click.on(SELECCION_AÑO_NACIMIENTO),
                    Click.on(MES_NACIMIENTO), Click.on(SELECCION_MES_NACIMIENTO), Click.on(SELECCION_DIA_NACIMIENTO),
                    Click.on(NACIONALIDAD), Click.on(CIUDAD_NACIONALIDDAD), Click.on(TIPO_DOCUMENTO), Click.on(SELECCION_TIPO_DOCUMENTO),
                    Enter.theValue(documentoPersonaUno).into(NUMERO_DOCUMENTO), Click.on(GENERO), Click.on(TIPO_GENERO), Click.on(PAIS_TELEFONO), Click.on(SELECCION_PAIS_TELEFONO),
                    Enter.theValue(numeroTelefonoUno).into(NUMERO_TELEFONO), Enter.theValue(emailUno).into(EMAIL), Enter.theValue(confirmarEmailUno).into(CONFIRMAR_EMAIL),
                    Click.on(OFERTAS_EMAIL), Click.on(SIGUIENTE_PERSONA));
        }
    }
    public static DatosUno datosPersonaUno(String nombrePersonaUno, String apellidoPersonaUno, String documentoPersonaUno, String numeroTelefonoUno, String emailUno, String confirmarEmailUno){
        return instrumented(DatosUno.class, nombrePersonaUno, apellidoPersonaUno, documentoPersonaUno, numeroTelefonoUno, emailUno, confirmarEmailUno);
    }
}

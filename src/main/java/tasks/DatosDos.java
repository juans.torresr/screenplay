package tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.questions.Visibility;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static userinterface.DatosPersonaDos.*;

public class DatosDos implements Task {
    String nombrePersonaDos;
    String apellidoPersonaDos;
    String documentoPersonaDos;
    public DatosDos(String nombrePersonaDos, String apellidoPersonaDos, String documentoPersonaDos){
        this.nombrePersonaDos = nombrePersonaDos;
        this.apellidoPersonaDos = apellidoPersonaDos;
        this.documentoPersonaDos = documentoPersonaDos;
    }
    @Override
    public <T extends Actor> void performAs(T actor) {
        if (Visibility.of(NOMBRE_PERSONA_DOS).viewedBy(actor).asBoolean()){
            actor.attemptsTo(Enter.theValue(nombrePersonaDos).into(NOMBRE_PERSONA_DOS), Enter.theValue(apellidoPersonaDos).into(APELLIDO_PERSONA_DOS),
                    Click.on(FECHA_NACIMIENTO), Click.on(AÑO_NACIMIENTO), Click.on(SELECCION_AÑO_NACIMIENTO), Click.on(MES_NACIMIENTO),
                    Click.on(SELECCION_MES_NACIMIENTO), Click.on(SELECCION_DIA_NACIMIENTO), Click.on(NACIONALIDAD), Click.on(CIUDAD_NACIONALIDDAD),
                    Click.on(TIPO_DOCUMENTO), Click.on(SELECCION_TIPO_DOCUMENTO), Enter.theValue(documentoPersonaDos).into(NUMERO_DOCUMENTO),
                    Click.on(GENERO), Click.on(TIPO_GENERO), Click.on(SIGUIENTE_CABINA), Click.on(SIGUIENTE_PERSONA));
        }
    }
    public static DatosDos datosPersonaDos(String nombrePersonaDos, String apellidoPersonaDos, String documentoPersonaDos){
        return instrumented(DatosDos.class, nombrePersonaDos, apellidoPersonaDos, documentoPersonaDos);
    }
}

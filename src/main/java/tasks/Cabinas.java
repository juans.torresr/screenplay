package tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import static net.serenitybdd.screenplay.Tasks.instrumented;
import static userinterface.CuantasCabinas.CANTIDAD_CABINAS;
import static userinterface.CuantasCabinas.SELECCION_CABINA;

public class Cabinas implements Task{
    String cabina;
    public Cabinas(String cabina){this.cabina = cabina;}
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(SELECCION_CABINA), Click.on(CANTIDAD_CABINAS));
    }
    public static Cabinas lasCabinas(String cabina){
        return instrumented(Cabinas.class, cabina);
    }
}

<html lang="es" class=""><head>
    	    <title>PeruRail - A luxury Peruvian Andes journey!</title>
    <meta charset="utf-8">
    <meta name="viewport" content="initial-scale=1">
                 <meta property="og:title" content="PeruRail">
             <meta property="og:description" content="Buy here your train tickets to Machu Picchu, Puno and Arequipa.">
             <meta property="og:image" content="https://www.perurail.com/wp-content/uploads/2017/07/pr-meta-image.jpg">
             <meta property="og:url" content="https://pax3.perurail.com/ecommerceaep/trenes?trenselect=2&amp;idioma=en">             
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@PeruRail_Trains">
<meta property="og:type" content="website">    <!-- Google Tag Manager -->
    <script async="" src="https://www.clarity.ms/eus-e/s/0.6.43/clarity.js"></script><script async="" src="https://www.clarity.ms/tag/uet/134621220"></script><script async="" src="https://www.clarity.ms/tag/9fyn27ucmh?ref=gtm2"></script><script src="https://connect.facebook.net/signals/config/220375522610602?v=2.9.87&amp;r=stable" async=""></script><script async="" src="https://connect.facebook.net/en_US/fbevents.js"></script><script type="text/javascript" async="" src="https://bat.bing.com/bat.js"></script><script type="text/javascript" async="" src="https://www.google-analytics.com/analytics.js"></script><script async="" src="https://www.googletagmanager.com/gtm.js?id=GTM-W8WG5M"></script><script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-W8WG5M');</script>
    <!-- End Google Tag Manager -->
    <link rel="stylesheet" href="https://pax3.perurail.com/ecommerceaep/assets/componentes/intl-tel-input/css/intlTelInput.css">
    <link rel="stylesheet" href="https://pax3.perurail.com/ecommerceaep/assets/css/main.css">
    <link rel="stylesheet" href="https://pax3.perurail.com/ecommerceaep/assets/css/zoom.css">
    <link rel="stylesheet" href="https://pax3.perurail.com/ecommerceaep/assets/css/lightbox.css" type="text/css">
	
	<!----->
    
                    <script>var BASEURL = 'https://pax3.perurail.com/ecommerceaep/';</script>
        <script>var SITEURL = 'https://pax3.perurail.com/ecommerceaep/';</script>
        <script>var CURRENT_PAGE = 'errorventas';</script>
        		
        <script>var FECMAXCAL_nobae = '';</script>
        <script>var FECMAXCAL_bae = '2023-01-03';</script>
        <script>var FECMINCAL = '0D';</script>
		
		<script>var GETIDIOMA = 'en';</script>
        <script>var tren_bae = '1';</script>
                    <script>var TIPOTREN = 'PERURAIL';</script>
            
        <script type="text/javascript" src="https://pax3.perurail.com/ecommerceaep/util"></script>
        <script type="text/javascript" src="https://pax3.perurail.com/ecommerceaep/util/trenes/6"></script>
        <!--[if lt IE 9]>
        <script src="js/min/html5shiv.min.js"></script>
        <![endif]-->
        
        
<meta http-equiv="origin-trial" content="A7bG5hJ4XpMV5a3V1wwAR0PalkFSxLOZeL9D/YBYdupYUIgUgGhfVJ1zBFOqGybb7gRhswfJ+AmO7S2rNK2IOwkAAAB7eyJvcmlnaW4iOiJodHRwczovL3d3dy5nb29nbGV0YWdtYW5hZ2VyLmNvbTo0NDMiLCJmZWF0dXJlIjoiUHJpdmFjeVNhbmRib3hBZHNBUElzIiwiZXhwaXJ5IjoxNjY5NzY2Mzk5LCJpc1RoaXJkUGFydHkiOnRydWV9"><meta http-equiv="origin-trial" content="A7bG5hJ4XpMV5a3V1wwAR0PalkFSxLOZeL9D/YBYdupYUIgUgGhfVJ1zBFOqGybb7gRhswfJ+AmO7S2rNK2IOwkAAAB7eyJvcmlnaW4iOiJodHRwczovL3d3dy5nb29nbGV0YWdtYW5hZ2VyLmNvbTo0NDMiLCJmZWF0dXJlIjoiUHJpdmFjeVNhbmRib3hBZHNBUElzIiwiZXhwaXJ5IjoxNjY5NzY2Mzk5LCJpc1RoaXJkUGFydHkiOnRydWV9"><script async="" src="//static.site24x7rum.com/beacon/site24x7rum-min.js?appKey=0f17cc150e78ad64ce62fd093f7c7c81"></script><script src="https://bat.bing.com/p/action/134621220.js" type="text/javascript" async="" data-ueto="ueto_84a76d9684"></script><style type="text/css">.fancybox-margin{margin-right:0px;}</style></head>
<body style="">  
  
  <!-- Google Tag Manager (noscript) -->
  <noscript>
     <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W8WG5M" height="0" width="0" style="display:none;visibility:hidden"></iframe>
  </noscript>
  <!-- End Google Tag Manager (noscript) -->
   <section id="wrapper">
    
        <article class="pasos">
        <div>
            <a>
                <span>1</span>
                <div>Search</div>
            </a>
        </div>
        <div class="espacio">
            <hr>
        </div>
        <div>
            <a>
                <span>2</span>
                <div>Choose your cabin </div>
            </a>
        </div>
        <div class="espacio">
            <hr>
        </div>
        <div>
            <a>
                <span>3</span>
                <div>Passenger's data </div>
            </a>
        </div>
        <div class="espacio">
            <hr>
        </div>
        <div>
            <a>
                <span>4</span>
                <div>CONFIRMATION AND PAYMENT</div>
            </a>
        </div>
    </article>

      <section class="contenedor-confirmacion-sin-ticket">

        <div class="mensaje-de-validacion">
          <h1>Important</h1>
          <div class="validado">
            The departures date entered is not valid, check your date selected          </div>
            <p>Please, make a new search for your purchase.</p>
        </div>
          <div class="botones clearfix" style="margin-right: 28.72px;">
              <a href="https://pax3.perurail.com/ecommerceaep/trenes/iniciar" class="seleccionar">Return</a>
          </div>
      </section>
  </section>
  
<footer>
    <img src="https://pax3.perurail.com/ecommerceaep/assets/images/paypal.jpg">
	<img src="https://pax3.perurail.com/ecommerceaep/assets/images/visa.jpg">
	<img src="https://pax3.perurail.com/ecommerceaep/assets/images/mastercar.jpg">
	<img src="https://pax3.perurail.com/ecommerceaep/assets/images/safety.jpg">
	<img src="https://pax3.perurail.com/ecommerceaep/assets/images/diners_logo.jpg">
	<img src="https://pax3.perurail.com/ecommerceaep/assets/images/amex_logo.jpg">	
	<span>This site is insured by: 	<img src="https://pax3.perurail.com/ecommerceaep/assets/images/seal_100-40_blue.png"></span>
</footer>
<!--Start of LiveBeep Script-->	
<script type="text/javascript">
    (function (d, s) {
        u = (('https:' == d.location.protocol) ? 'https://' : 'http://') + 'www.livebeep.com/eye.js?duk=iEiq79uC8HM&lang=en';
        if ((h = d.location.href.split(/#ev!/)[1]))
            u += '?_e=' + h;
        else if ((r = /.*\_evV=(\w+)\b.*/).test(c = d.cookie))
            u += '?_v=' + c.replace(r, '$1');
        d.write(unescape('%3Cscript src="' + u + '" type="text/javascript"%3E%3C/script%3E'));
    })(document, 'script');
</script><iframe height="0" width="0" style="display: none; visibility: hidden;" src="//11714337.fls.doubleclick.net/activityi;src=11714337;type=rmkt-0;cat=rmktn0;ord=5590979652059;gtm=2wgaj0;auiddc=1830233369.1666470562;u1=https%3A%2F%2Fpax3.perurail.com%2Fecommerceaep%2Ferrorventas;~oref=https%3A%2F%2Fpax3.perurail.com%2Fecommerceaep%2Ferrorventas?"></iframe><script src="https://www.livebeep.com/eye.js?duk=iEiq79uC8HM&amp;lang=en" type="text/javascript"></script><script type="text/javascript" id="" src="//rum-static.pingdom.net/pa-5e3d33a011c07000080003b0.js"></script>
<script type="text/javascript" id="">!function(b,e,f,g,a,c,d){b.fbq||(a=b.fbq=function(){a.callMethod?a.callMethod.apply(a,arguments):a.queue.push(arguments)},b._fbq||(b._fbq=a),a.push=a,a.loaded=!0,a.version="2.0",a.queue=[],c=e.createElement(f),c.async=!0,c.src=g,d=e.getElementsByTagName(f)[0],d.parentNode.insertBefore(c,d))}(window,document,"script","https://connect.facebook.net/en_US/fbevents.js");fbq("init","220375522610602");fbq("track","PageView");</script>
<noscript>
 <img height="1" width="1" src="https://www.facebook.com/tr?id=220375522610602&amp;ev=PageView
&amp;noscript=1">
</noscript>
<script type="text/javascript" id="">(function(a,e,b,f,g,c,d){a[b]=a[b]||function(){(a[b].q=a[b].q||[]).push(arguments)};c=e.createElement(f);c.async=1;c.src="https://www.clarity.ms/tag/"+g+"?ref\x3dgtm2";d=e.getElementsByTagName(f)[0];d.parentNode.insertBefore(c,d)})(window,document,"clarity","script","9fyn27ucmh");</script><script type="text/javascript" id="">(function(a,f,h,b,k,c,e){a.performance&&a.performance.timing&&a.performance.navigation&&(a[b]=a[b]||function(){(a[b].q=a[b].q||[]).push(arguments)},c=f.createElement("script"),c.async=!0,c.setAttribute("src",h+k),f.getElementsByTagName("head")[0].appendChild(c),e=window.onerror,window.onerror=function(g,l,m,n,d){e&&e(g,l,m,n,d);d||(d=Error(g));(a[b].q=a[b].q||[]).push(["captureException",d])})})(window,document,"//static.site24x7rum.com/beacon/site24x7rum-min.js?appKey\x3d","s247r","0f17cc150e78ad64ce62fd093f7c7c81");</script>
<!--End of LiveBeep Script-->	<script src="https://pax3.perurail.com/ecommerceaep/assets/js/lightbox-plus-jquery.js"></script>
    <script>
        lightbox.option({
            maxWidth: 600,
            maxHeight: 600,
            disableScrolling: true
        });
    </script>
	<script src="https://pax3.perurail.com/ecommerceaep/assets/js/recursos-min.js"></script>
	<script src="https://pax3.perurail.com/ecommerceaep/assets/componentes/intl-tel-input/js/intlTelInput.js"></script>
    <script src="https://pax3.perurail.com/ecommerceaep/assets/js/main.js"></script>
    <script src="https://pax3.perurail.com/ecommerceaep/assets/js/validate.js"></script>
    <script src="https://pax3.perurail.com/ecommerceaep/assets_mdp/scripts/trenes-bae.js"></script>
    <script src="https://pax3.perurail.com/ecommerceaep/assets_mdp/scripts/elige-tren.js"></script>
    <script src="https://pax3.perurail.com/ecommerceaep/assets_mdp/scripts/pasajeros-bae.js"></script>

    
<script>
    $(document).ready(function () {
        
        $.datepicker.setDefaults($.datepicker.regional[""]);

        $(".calendaryear").datepicker({
            dateFormat: 'dd-mm-yy',
            minDate: "23-10-1922",
            maxDate: "22-10-2010",
            yearRange: '1922:2010',
            defaultDate: "01-01-1972",
            changeMonth: true,
            changeYear: true,
            onSelect: function (dateText) {
                $(".calendaryear").removeClass("required");
                $(".calendaryear").valid();
            },
            onChangeMonthYear: function (y, m, i) {
                var diac = i.selectedDay;
                var mesc = i.selectedMonth;
                var anoc = i.selectedYear;
                $(this).datepicker("setDate", new Date(anoc, mesc, diac));
            },
            onClose: function (dateText, inst) {
                var toDate = new Date(inst.selectedYear, inst.selectedMonth, inst.selectedDay);
                $(this).datepicker('setDate', new Date(toDate));
            }
        });

        $(".calendaryearnino").datepicker({
            dateFormat: 'dd-mm-yy',
            minDate: "23-10-2010",
            maxDate: "22-10-2022",
            yearRange: '2010:2022',
            defaultDate: "01-01-2016",
            changeMonth: true,
            changeYear: true,
            onSelect: function (dateText) {
                $(".calendaryearnino").removeClass("required");
                $(".calendaryearnino").valid();
            },
            onChangeMonthYear: function (y, m, i) {
                var diac = i.selectedDay;
                var mesc = i.selectedMonth;
                var anoc = i.selectedYear;
                $(this).datepicker("setDate", new Date(anoc, mesc, diac));
            },
            onClose: function (dateText, inst) {
                var toDate = new Date(inst.selectedYear, inst.selectedMonth, inst.selectedDay);
                $(this).datepicker('setDate', new Date(toDate));
            }
        });

        
        var disabledDays = ["10-5-2015"];

        function disableAllTheseDays(date) {
            var m = date.getMonth(), d = date.getDate(), y = date.getFullYear();
            for (i = 0; i < disabledDays.length; i++) {
                if ($.inArray(d + '-' + (m + 1) + '-' + y, disabledDays) != -1) {
                    return [false];
                }
            }
            return [true];
        }
                //        $(".calendar_buffet").datepicker({
//            dateFormat: 'dd-mm-yy',
//            minDate: "//",
//            maxDate: "//",
//            beforeShowDay: disableAllTheseDays
//        });
        //        $(".calendar_buffet").datepicker({
//            dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
//            dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
//            monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
//            monthNamesShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
//            currentText: 'Hoy',
//            nextText: 'Sig.',
//            prevText: 'Ant.',
//            dateFormat: 'dd-mm-yy',
//            minDate: "//",
//            maxDate: "//",
//            beforeShowDay: disableAllTheseDays
//        });
            });
</script><!--
<script src="https://pax3.perurail.com/ecommerceaep/js/jquery-1.10.2.min.js"></script>
<script src="https://pax3.perurail.com/ecommerceaep/js/jquery-ui-1.10.4.custom.min.js"></script>
<script src="https://pax3.perurail.com/ecommerceaep/js/jquery.customSelect.js"></script>
<script src="https://pax3.perurail.com/ecommerceaep/js/jquery.ui.datepicker-en.js"></script>
<script src="https://pax3.perurail.com/ecommerceaep/js/conditionizr.min.js"></script>
<script src="https://pax3.perurail.com/ecommerceaep/js/validate.js"></script>
<script src="https://pax3.perurail.com/ecommerceaep/js/jquery.tablesorter.min.js"></script>
<script src="https://pax3.perurail.com/ecommerceaep/js/main.js"></script>

<script src="https://code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script src="https://pax3.perurail.com/ecommerceaep/js/calendario.js"></script>
-->





<!--
<script type="text/javascript" src="https://pax3.perurail.com/ecommerceaep/js/jquery-validate/additional-methods-custom.js"></script>
<script type="text/javascript" src="https://pax3.perurail.com/ecommerceaep/assets_mdp/scripts/elige-tren.js"></script>
<script type="text/javascript" src="https://pax3.perurail.com/ecommerceaep/assets_mdp"></script>
-->






<div id="lightboxOverlay" class="lightboxOverlay" style="display: none;"></div><div id="lightbox" class="lightbox" style="display: none;"><div class="lb-outerContainer"><div class="lb-container"><img class="lb-image" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=="><div class="lb-nav"><a class="lb-prev" href=""></a><a class="lb-next" href=""></a></div><div class="lb-loader"><a class="lb-cancel"></a></div></div></div><div class="lb-dataContainer"><div class="lb-data"><div class="lb-details"><span class="lb-caption"></span><span class="lb-number"></span></div><div class="lb-closeContainer"><a class="lb-close"></a></div></div></div></div><div style="width:0px; height:0px; display:none; visibility:hidden;" id="batBeacon518199605741"><img style="width:0px; height:0px; display:none; visibility:hidden;" id="batBeacon245452291355" width="0" height="0" alt="" src="https://bat.bing.com/action/0?ti=134621220&amp;tm=gtm002&amp;Ver=2&amp;mid=86887906-d005-4cb8-b32d-17a41fe301f9&amp;sid=365034a0524811eda2c1d159131e6947&amp;vid=365053c0524811ed8060036a0565e89f&amp;vids=0&amp;msclkid=N&amp;uach=pv%3D14.0.0&amp;pi=918639831&amp;lg=es-419&amp;sw=1366&amp;sh=768&amp;sc=24&amp;nwd=1&amp;tl=PeruRail%20-%20A%20luxury%20Peruvian%20Andes%20journey!&amp;p=https%3A%2F%2Fpax3.perurail.com%2Fecommerceaep%2Ferrorventas&amp;r=https%3A%2F%2Fwww.perurail.com%2F&amp;lt=2836&amp;evt=pageLoad&amp;sv=1&amp;rn=609133"></div><div id="sb-container"><div id="sb-overlay"></div><div id="sb-wrapper"><div id="sb-title"><div id="sb-title-inner"></div></div><div id="sb-wrapper-inner"><div id="sb-body"><div id="sb-body-inner"></div><div id="sb-loading"><div id="sb-loading-inner"><span>loading</span></div></div></div></div><div id="sb-info"><div id="sb-info-inner"><div id="sb-counter"></div><div id="sb-nav"><a id="sb-nav-close" title="Close" onclick="Shadowbox.close()"></a><a id="sb-nav-next" title="Next" onclick="Shadowbox.next()"></a><a id="sb-nav-play" title="Play" onclick="Shadowbox.play()"></a><a id="sb-nav-pause" title="Pause" onclick="Shadowbox.pause()"></a><a id="sb-nav-previous" title="Previous" onclick="Shadowbox.previous()"></a></div></div></div></div></div></body></html>